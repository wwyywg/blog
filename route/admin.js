// 引入express框架
const express = require('express')
// 创建路由
const admin = express.Router()

admin.get('/login', (req, res) => {
    
    res.render('admin/login')
})

admin.get('/user', (req, res) => {

    res.render('admin/user')
})

// 将路由对象做为模块成员导出
module.exports = admin