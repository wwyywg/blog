// 引入express框架
const express = require('express')
// 创建路由
const home = express.Router()

home.get('/', (req, res) => {
    res.send('欢迎来到博客首页')
})

// 将路由对象做为模块成员导出
module.exports = home